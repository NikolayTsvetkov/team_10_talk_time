import React, { createContext, useContext, useState } from 'react';

const ValidationContext = createContext();

const keysValidation = {
    'auth/invalid-email': 'You entered invalid email! Please check!',
    'auth/user-not-found': 'We did not find the user in out system',
    'auth/wrong-password': 'You entered wrong password! Please check!',
    'auth/email-already-in-use': 'The use is alread registered! Try login!'
}

export const useValidation = () => useContext(ValidationContext);

export const ValidationProvider = ({ children }) => {
    const [errors, setErrors] = useState([]);

    const addError = (errorMessage) => {
        if (keysValidation?.[errorMessage]) {
            return setErrors((prevErrors) => [...prevErrors, keysValidation[errorMessage]]);
        }
        return setErrors((prevErrors) => [...prevErrors, errorMessage]);
    };

    const clearErrors = () => {
        setErrors([]);
    };

    return (
        <ValidationContext.Provider value={{ errors, addError, clearErrors }}>
            {children}
        </ValidationContext.Provider>
    );
};
