import React, { createContext, useContext, useEffect, useState } from "react";
import { auth } from "../config/firebase-config";
import { getUserByUid, getUserPersonalMessagesUid } from "../services/users.service";
import { changesListenerCurrentUserChat } from "../services/chat.service";

import { onValue, ref } from "firebase/database";
import { db } from "../config/firebase-config";

const AuthContext = createContext();
export function useAuth() {
    return useContext(AuthContext);
}

export function AuthProvider({ children }) {
    const [currentUser, setCurrentUser] = useState(() => {
        const storedUser = localStorage.getItem("currentUser");
        return storedUser ? JSON.parse(storedUser) : null;
    });
    const [currentUserFullInfo, setCurrentUserFullInfo] = useState({});
    const [usersByLevel, setUsersByLevel] = useState([]);
    const [userPersonalMessages, setUserPersonalMessages] = useState([]);
    const [level, setLevel] = useState('A1')

    useEffect(() => {
        if (currentUser) {
            fetchUserFullInfo(currentUser);
            fetchUserPersonalMessages(currentUser);
            changesListenerCurrentUserChat(currentUser, setUserPersonalMessages);
            fetchUserPersonalMessages(userPersonalMessages);

            const unsubscribe = onValue(ref(db, `users/${currentUser?.uid}/fields`), (snapshot) => {
                const data = snapshot.val();
                //console.log(data, '<---------- the data for the new currentUserFullInfo')
                setCurrentUserFullInfo(data);
            });
            return () => unsubscribe(); // Cleanup the listener when component unmounts
        }
    }, [currentUser, userPersonalMessages]);

    const signup = (email, password) => {
        return auth.createUserWithEmailAndPassword(email, password);
    };

    const login = (email, password) => {
        return auth.signInWithEmailAndPassword(email, password);
    };

    const logout = () => {
        auth.signOut();
        setCurrentUser(null);
        localStorage.removeItem("currentUser");
    };

    const handleSetCurrentUser = async (user) => {
        const userDb = await getUserByUid(user.uid);
        const currentUserData = { ...user, ...userDb };
        setCurrentUser(currentUserData);
        localStorage.setItem("currentUser", JSON.stringify(currentUserData));
    };

    const handleSetUsersByLevel = async (users) => setUsersByLevel(users);

    const fetchUserFullInfo = async (user) => {
        const result = await getUserByUid(user.uid);
        setCurrentUserFullInfo(result);
    };

    // const fetchUserPersonalMessages = async (user) => {
    //     const result = await getUserPersonalMessagesUid(user?.uid);
    //     setUserPersonalMessages(result);
    // };
    const fetchUserPersonalMessages = async (messages) => {
        // const result = await getUserPersonalMessagesUid(messages);
        setUserPersonalMessages(messages);
      };

    const value = {
        currentUser,
        currentUserFullInfo,
        usersByLevel,
        userPersonalMessages,
        login,
        signup,
        logout,
        handleSetCurrentUser,
        handleSetUsersByLevel,
        level,
        setLevel
    };

    return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
}
