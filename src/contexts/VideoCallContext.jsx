import React, { createContext, useContext, useRef, useState, useEffect } from 'react';

const VideoCallContext = createContext();

export const VideoCallProvider = ({ children }) => {
    const [isVideoCallActive, setIsVideoCallActive] = useState(false);
    const localVideoRef = useRef(null);
    const remoteVideoRef = useRef(null);
    const [localStream, setLocalStream] = useState(null);
    const [peerConnection, setPeerConnection] = useState(null);

    useEffect(() => {
        if (localVideoRef.current && localStream) {
            localVideoRef.current.srcObject = localStream;
        }
    }, [localStream]);

    const startVideoCall = async () => {
        try {
            const stream = await navigator.mediaDevices.getUserMedia({ video: true, audio: true });
            setLocalStream(stream);
            setIsVideoCallActive(true);
            createPeerConnection(); // Create the peer connection when starting the call
        } catch (error) {
            console.error('Error accessing media devices:', error);
        }
    };

    const endVideoCall = () => {
        if (localStream) {
            localStream.getTracks().forEach((track) => track.stop());
        }
        if (localVideoRef.current) {
            localVideoRef.current.srcObject = null;
        }
        if (peerConnection) {
            peerConnection.close();
            setPeerConnection(null);
        }
        setIsVideoCallActive(false);
    };

    const createPeerConnection = () => {
        const pc = new RTCPeerConnection();
        pc.addEventListener('icecandidate', handleIceCandidate);
        pc.addEventListener('track', handleTrack);
        if (localStream) {
            localStream.getTracks().forEach((track) => pc.addTrack(track, localStream));
        }
        setPeerConnection(pc); // Store the peer connection in state
    };

    const handleIceCandidate = (event) => {
        if (event.candidate) {
            // Send the candidate to the other peer using your signaling mechanism
        }
    };

    const handleTrack = (event) => {
        if (remoteVideoRef.current) {
            remoteVideoRef.current.srcObject = event.streams[0];
        }
    };

    const contextValue = {
        isVideoCallActive,
        localVideoRef,
        remoteVideoRef,
        startVideoCall,
        endVideoCall,
        createPeerConnection,
    };

    return (
        <VideoCallContext.Provider value={contextValue}>
            {children}
        </VideoCallContext.Provider>
    );
};

export const useVideoCall = () => useContext(VideoCallContext);
