import { get, onValue, push, ref } from "firebase/database";
import { db } from "../config/firebase-config";
import { getUsersByLevel } from "./users.service";

export const fetchUsersByLevel = async (level) => {
    console.log(level);

    try {
        const usersByLevel = await getUsersByLevel(level);
        console.log(usersByLevel);

        return usersByLevel
    } catch (err) {
        console.log(err);

    }
}

export const addChatMessage = async (message, level) => {


    try {
        const newMessages = await push(ref(db, `chatByUsersLevel/${level}`), message);
        return newMessages
    } catch (error) {
        console.log(error);

    }
}

export const getUsersByChatLevel = async (level) => {
    try {
        const levelsRef = ref(db, `chatByUsersLevel/${level}`)

        onValue(levelsRef, (snapshot) => {
            const messages = Object.values(snapshot.val());
            return messages
        });

    } catch (error) {
        console.log(error);

    }
}