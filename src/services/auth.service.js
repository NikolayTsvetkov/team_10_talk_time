import { createUserWithEmailAndPassword, signInWithEmailAndPassword, signOut, getAuth } from 'firebase/auth';
import { createUserDB, getUserByUid } from '../services/users.service'

const auth = getAuth();

export const registerUser = async (fields) => {
    const { email, password } = fields;

    try {
        const userCredential = await createUserWithEmailAndPassword(auth, email, password);
        const { uid } = userCredential.user;

        await createUserDB(uid, fields);


        const userObj = {
            reloadUserInfo: userCredential.reloadUserInfo,
            uid: uid
        };

        return userObj;
    } catch (error) {
        const errorCode = error.code;
        const errorMessage = error.message;
        throw error
    }

};

export const loginUser = async (email, password) => {
    try {
        const userCredential = await signInWithEmailAndPassword(auth, email, password);
        const { reloadUserInfo, uid } = userCredential.user;
        const userInfo = await getUserByUid(uid)
        const userObj = {
            ...userInfo, uid, status: 'online'
        }
        return { userObj };
    } catch (error) {
        console.log('Login error:', error);
        throw error;
    }
};


export const logoutUser = () => {
    return signOut(auth);
};
