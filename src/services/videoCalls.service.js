import { get, ref, query, equalTo, push, set, orderByChild } from 'firebase/database';
import { db } from "../config/firebase-config";

export const sendInvitation = async (invitation, currentUser) => {
    console.log(invitation)

    try {
        const { sender, receiver, participants, title, date } = invitation;

        const videoCall = {
            sender: currentUser.uid,
            receiver,
            participants,
            title,
            date: date.toString(),
        };

        const month = date.getMonth() + 1;
        const year = date.getFullYear();
        const videoCallsRef = ref(db, 'videoCalls');
        const newVideoCallRef = push(videoCallsRef);
        await set(ref(db, `videoCalls/${year}/${month}/${newVideoCallRef.key}`), videoCall);

        const participantsQuery = query(
            videoCallsRef,
            orderByChild('participants').equalTo(participants),
            orderByChild('date').equalTo(date)
        );

        const snapshot = await get(participantsQuery);

        if (snapshot.exists()) {
            const videoCalls = [];
            snapshot.forEach((childSnapshot) => {
                const videoCall = childSnapshot.val();
                videoCalls.push(videoCall);
            });
            return videoCalls;
        }

        return null;
    } catch (error) {
        console.log(error);
        return error
    }
};

export const getMeetingsByMonth = async (month, year, currentUser) => {
    try {
        const videoCallsRef = ref(db, `videoCalls/${year}/${month}`);
        // console.log(videoCallsRef);

        const participantsQuery = query(
            videoCallsRef,
            orderByChild('participants'),
            equalTo(currentUser.uid)
        );

        const snapshot = await get(videoCallsRef);
        console.log(snapshot.val());

        if (snapshot.exists()) {
            const videoCalls = [];
            snapshot.forEach((childSnapshot) => {
                const videoCall = childSnapshot.val();
                videoCalls.push(videoCall);
            });
            return videoCalls;
        }

        return [];
    } catch (error) {
        console.log(error);
        return error;
    }
};