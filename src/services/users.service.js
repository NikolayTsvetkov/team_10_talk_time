
import { get, ref, set, orderByChild, equalTo, query, orderByKey, orderByValue, update } from "firebase/database";
import { db } from "../config/firebase-config";


export const createUserDB = (uid, fields) => {
  return set(ref(db, `users/${uid}`), { uid, fields, createdOn: new Date() })
};

export const getUserDB = (uid) => {
  return get(ref(db, `users/${uid}`));
};

export const getUserByUid = async (uid) => {
  try {
    const snapshot = await get(ref(db, `users/${uid}`));
    if (snapshot.exists()) {
      const userData = snapshot.val().fields;
      return userData;
    }

  } catch (error) {
    console.error(error);
  }
};


export const getOnlineUsers = async () => {
  try {
    const snapshot = await get(ref(db, 'users'));
    const onlineUsers = [];

    snapshot.forEach((userSnapshot) => {
      const userData = userSnapshot.val();
      if (userData?.fields?.status && userData?.fields?.status === 'online') {
        onlineUsers.push(userData);
      }
    });

    return onlineUsers;

  } catch (error) {
    console.error(error);
  }
};

export const getUserPersonalMessagesUid = async (uid) => {
  try {
    // const snapshot = await get(ref(db, `users/${uid}`));
    // if (snapshot.exists()) {
    //   const userData = snapshot.val().messages;
    console.log('Object.keys(msgs) ', msgs);
    return Object.keys(msgs);
    // }

  } catch (error) {
    console.error(error);
  }
};

export const getAllUsers = async () => {
  const dbRef = ref(db, 'users');
  const snapshot = await get(dbRef);

  const users = Object.values(snapshot.val() || {})
  return users;
};

export const getUsersByLevel = async (level) => {
  const dbRef = ref(db, 'users');
  const snapshot = await get(dbRef, orderByValue('fields/level'), equalTo(level));

  const users = [];
  Object.values(snapshot.val()).forEach((childSnapshot) => {
    if (childSnapshot?.fields?.level && childSnapshot?.fields?.level === level) {
      users.push(childSnapshot)
    }

  });

  return users;
};

export const searchUserByName = async (name) => {
  const dbRef = ref(db, 'users');
  const snapshot = await get(dbRef, orderByValue('fields/username'), equalTo(name));

  const users = [];
  Object.values(snapshot.val()).forEach((childSnapshot) => {
    if (childSnapshot?.fields?.username && childSnapshot?.fields?.username === name) {
      users.push(childSnapshot)
    }

  });

  return users;
};

export const updateUserUniversal = (handle, key, value) => {
  const updatedUser = {}
  updatedUser[`users/${handle}/${key}`] = value
  return update(ref(db), updatedUser)
}

export const chatUsersSearch = async (uid) => {
  const userRef = doc(db, "users", uid);

  try {
    const userSnapshot = await getDoc(userRef);
    if (!userSnapshot.exists()) {
      return null;
    }

    const user = {
      uid: userSnapshot.id,
      ...userSnapshot.data().fields
    };

    return user;
  } catch (error) {
    console.error(error);
    return null;
  }
};

export const getUserPhotoURL = async (userUID) => {
  const userSnapshot = await get(ref(db, `users/${userUID}`));
  return userSnapshot.val()?.fields?.photoURL;
};