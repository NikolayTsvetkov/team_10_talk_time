import {
    EmailIcon,
    CalendarIcon,
    EditIcon,
    LinkIcon,
    QuestionOutlineIcon,
    SunIcon,
    InfoIcon,
    DragHandleIcon,
    AttachmentIcon,
} from '@chakra-ui/icons';


export const sidebarDataTop = [
    {
        id: 1,
        name: "Personal Messages",
        icon: "fa-solid fa-envelope",
        link: '/chat'
    },
    {
        id: 2,
        name: "Levels",
        icon: "fa-solid fa-chalkboard-user",
        link: "levels"
    },
    {
        id: 3,
        name: "Talk groups",
        icon: "fa-solid fa-people-group",
    },
    {
        id: 4,
        name: "Calendar",
        icon: "fa-solid fa-calendar-alt",
        link: '/calendar'
    },
    // {
    //     id: 5,
    //     name: "More...",
    //     icon: DragHandleIcon,
    // },
];

export const sidebarDataBottom = [
    // {
    //     id: 6,
    //     name: "Help",
    //     icon: QuestionOutlineIcon,
    // },
    {
        id: 7,
        name: "About",
        icon: InfoIcon,
        team: [
            { id: 1, name: "Gogi" },
            { id: 2, name: "Niki" },
            { id: 3, name: "Rado" },
        ],
    },
];

export const leftSidebarChildData = [
    {
        id: 1,
        level: "A1",
        icon: AttachmentIcon,
    },
    {
        id: 2,
        level: "A2",
        icon: AttachmentIcon,
    },
    {
        id: 3,
        level: "B1",
        icon: AttachmentIcon,
    },
    {
        id: 4,
        level: "B2",
        icon: AttachmentIcon,
    },
    {
        id: 5,
        level: "C1",
        icon: AttachmentIcon,
    },
    {
        id: 6,
        level: "C2",
        icon: AttachmentIcon,
    },
    //Beginner, Pre-intermediate,Intermediate, Advanced, Proficient
];

export const chanelConversations = [
    {
        id: 1,
        theme: "Present Simple Tense and more text here",
        conversation: [{ me: 'Hi hi hi' }, { him: 'Xxx xxxx xxxx' }],
        icon: { name: 'Segun Adebayo', src: 'https://bit.ly/sage-adebayo' },
    },
    {
        id: 2,
        theme: "Verb",
        conversation: [{ me: 'Xo xo xo' }, { him: 'Xxx xxxx xxxx' }],
        icon: { name: 'Kent Dodds', src: 'https://bit.ly/kent-c-dodds' },
    },
    {
        id: 3,
        theme: "Examples",
        icon: { name: 'Kent Dodds', src: 'https://bit.ly/kent-c-dodds' },
    },
    {
        id: 4,
        theme: "B2 Again",
        icon: { name: 'Kent Dodds', src: 'https://bit.ly/kent-c-dodds' },
    },
];

export const messagesTest = [
    { msg1: "-NW7ns0kAFP13Ed8DCdh" },
    { msg2: "-NW7s6ZHcf4T-12LwYWn" },
    { msg3: "-NW7sMzzi0YvXigisnfL" },
    { msg4del: "-NW7sr10CJkgUbba4wSu" },
]
