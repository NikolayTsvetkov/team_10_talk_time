import React, { useRef, useState } from 'react';
import { Box, Button, CloseButton, Input, Text, VStack, HStack } from '@chakra-ui/react';
import { useVideoCall } from '../../contexts/VideoCallContext';
import { useEffect } from 'react';

const InvitationBox = ({ onClose, onSubmit, pickedDay, meetings }) => {

    const [invitationTitle, setInvitationTitle] = useState('');
    const [invitationEmails, setInvitationEmails] = useState([]);
    const colors = ['teal.200', 'blue.200', 'green.200', 'purple.200'];
    const [schedule, setSchedule] = useState([])

    useEffect(() => {

        if (meetings) {
            const n = meetings.filter(meeting => Date.parse(meeting?.date) === Date.parse(pickedDay))
            setSchedule(n)
        }

    }, [meetings])
    const {
        isVideoCallActive,
        localVideoRef,
        remoteVideoRef,
        startVideoCall,
        endVideoCall,
        createPeerConnection,
    } = useVideoCall();

    const handleInvitationSubmit = () => {
        onSubmit(invitationTitle, invitationEmails, pickedDay);
        setInvitationTitle('');
        setInvitationEmails([]);
    };

    const handleVideoCall = () => {
        if (isVideoCallActive) {
            endVideoCall();
        } else {
            createPeerConnection();
            startVideoCall();
        }
    };

    const handleEmailChange = (index, value) => {
        const emails = [...invitationEmails];
        emails[index] = value;
        setInvitationEmails(emails);
    };

    const handleAddParticipant = () => {
        setInvitationEmails([...invitationEmails, '']);
    };

    const handleRemoveParticipant = (index) => {
        const emails = [...invitationEmails];
        emails.splice(index, 1);
        setInvitationEmails(emails);
    };

    const isFormValid = invitationTitle.trim() === '' || invitationEmails.some((email) => email.trim() === '');

    return (
        <Box
            bgGradient="linear(to-r, purple.800, black)"
            p={4}
            rounded="md"
            width="55%"
            position="absolute"
            right={0}
            top={0}
            boxShadow="0 0 10px rgba(0, 0, 0, 0.2)"
            zIndex={1}
        >
            <CloseButton position="absolute" right={2} top={2} onClick={onClose} />
            <Text fontSize="xl" fontWeight="bold" mb={4} color="white">
                Send Invitation
            </Text>
            <Text
                mb={2}
                color="white"
                fontWeight="bold"
                fontSize="lg"
                _hover={{
                    textShadow: '2px 2px 4px rgba(255, 255, 255, 0.8)',
                    transform: 'scale(1.05)',
                    transition: 'all 0.3s',
                }}
            >
                {pickedDay && pickedDay.toDateString()}
            </Text>
            <Input
                placeholder="Title"
                value={invitationTitle}
                onChange={(e) => setInvitationTitle(e.target.value)}
                color="white"
                mb={2}
            />
            {invitationEmails.map((email, index) => (
                <Input
                    key={index}
                    placeholder={`Participant ${index + 1} email`}
                    value={email}
                    onChange={(e) => handleEmailChange(index, e.target.value)}
                    color="white"
                    mb={2}
                />
            ))}
            <Button
                colorScheme="purple"
                onClick={handleAddParticipant}
                mb={2}
                _hover={{ backgroundColor: 'purple.600' }}
            >
                Add Participant
            </Button>
            <Button
                colorScheme="purple"
                onClick={handleInvitationSubmit}
                mb={2}
                _hover={{ backgroundColor: 'purple.600' }}
                disabled={isFormValid}
            >
                Send Invitation
            </Button>
            <Button
                colorScheme="purple"
                onClick={handleVideoCall}
                _hover={{ backgroundColor: 'purple.600' }}
            >
                {isVideoCallActive ? 'End Video Call' : 'Start Video Call'}
            </Button>
            <VStack spacing={4} align="stretch">
                {schedule.map((item, index) => (
                    <Box
                        key={index}
                        bg={colors[index % colors.length]}
                        borderWidth="1px"
                        borderRadius="md"
                        p={4}
                        boxShadow="md"
                    >
                        <Text fontSize="lg" fontWeight="bold" mb={2}>
                            {item.title}
                        </Text>
                        <Text mb={2}>
                            <strong>Date:</strong> {new Date(item.date).toDateString()}
                        </Text>
                        <HStack spacing={2}>
                            <Text>
                                <strong>Participants:</strong> {item.participants.join(', ')}
                            </Text>
                            <Text>
                                <strong>Receiver:</strong> {item.receiver.join(', ')}
                            </Text>
                            <Text>
                                <strong>Sender:</strong> {item.sender}
                            </Text>
                        </HStack>
                    </Box>
                ))}
            </VStack>
            {isVideoCallActive && (
                <Box mt={4}>
                    <Text fontWeight="bold" color="white">
                        Local Video:
                    </Text>
                    <video ref={localVideoRef} autoPlay playsInline style={{ width: '100%', height: 'auto' }} />
                    <Text fontWeight="bold" mt={2} color="white">
                        Remote Video:
                    </Text>
                    <video ref={remoteVideoRef} autoPlay playsInline style={{ width: '100%', height: 'auto' }} />
                </Box>
            )}
        </Box>
    );
};

export default InvitationBox;
