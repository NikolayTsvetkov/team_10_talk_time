import React, { useState, useEffect } from 'react';
import { Box, Grid, Text, ChakraProvider, extendTheme, Button, Input } from '@chakra-ui/react';
import InvitationBox from './InvitationBox';
import { VideoCallProvider } from '../../contexts/VideoCallContext';
import { searchUserByName } from '../../services/users.service';
import { getMeetingsByMonth, sendInvitation } from '../../services/videoCalls.service';
import { useAuth } from '../../contexts/AuthContext';
import { useValidation } from '../../contexts/ValidationCOntext';
// import { useEffect } from 'react';


const theme = extendTheme({
    colors: {
        primary: '#6B46C1',
        secondary: '#000000',
        tertiary: '#D1D5DB',
        opposite: '#FFFFFF',
        textPalette: ['#CBA7E2', '#D4A8FF', '#E4ABFF', '#F1AEFF'],
    },
});

const Calendar = () => {
    const { addError } = useValidation();
    const { currentUser } = useAuth();
    const currentDate = new Date();
    // console.log(currentUser)
    const [displayedMonth, setDisplayedMonth] = useState(currentDate);

    const [showInvitationBox, setShowInvitationBox] = useState(false);

    const [pickedDay, setPickedDay] = useState(null)

    const [meetings, setMeetings] = useState([]);

    useEffect(() => {
        const fetchMeetings = async () => {
            const month = displayedMonth.getMonth() + 1;
            const year = displayedMonth.getFullYear();

            try {
                const meetings = await getMeetingsByMonth(month, year, currentUser);
                setMeetings(meetings);
            } catch (error) {
                console.log('Error fetching meetings:', error);
            }
        };

        fetchMeetings();
    }, [displayedMonth, currentUser]);


    const goToPreviousMonth = () => {
        setDisplayedMonth(new Date(displayedMonth.getFullYear(), displayedMonth.getMonth() - 1, 1));
    };

    const goToNextMonth = () => {
        setDisplayedMonth(new Date(displayedMonth.getFullYear(), displayedMonth.getMonth() + 1, 1));
    };

    const handleInvitationSubmit = async (invitationTitle, invitationName, pickedDay) => {
        try {
            const user = await searchUserByName(invitationName);
            if (user) {
                const invitation = {
                    sender: currentUser.uid,
                    receiver: invitationName,
                    date: pickedDay,
                    title: invitationTitle,
                    participants: [...invitationName, currentUser.username],
                };

                // console.log(invitation);
                sendInvitation(invitation, currentUser);
            } else {
                addError('Participant name was not found');
            }
        } catch (error) {
            addError(error);
        }
        setShowInvitationBox(false);
    };


    const displayedMonthName = displayedMonth.toLocaleString('default', { month: 'long' });

    const totalDaysInMonth = new Date(displayedMonth.getFullYear(), displayedMonth.getMonth() + 1, 0).getDate();

    const firstDayOfMonth = new Date(displayedMonth.getFullYear(), displayedMonth.getMonth(), 1).getDay();

    const daysInMonth = Array.from({ length: totalDaysInMonth }, (_, index) => index + 1);

    const isParticipant = (meeting) => {
        // console.log(meeting?.participants)
        return meeting?.participants
            .includes(currentUser.username);
    };

    return (
        <ChakraProvider theme={theme}>
            <VideoCallProvider>
                <Box
                    bgGradient="linear(to-b, primary, tertiary)"
                    minH="100vh"
                    p={4}
                    rounded="md"
                    display="flex"
                    flexDirection="column"
                    alignItems="center"
                >
                    <Box display="flex" alignItems="center" justifyContent="space-between" mb={4} width="100%">
                        <Button colorScheme="primary" onClick={goToPreviousMonth}>
                            Previous Month
                        </Button>
                        <Text color="primary" fontSize="xl" fontWeight="bold">
                            {displayedMonthName}
                        </Text>
                        <Button colorScheme="primary" onClick={goToNextMonth}>
                            Next Month
                        </Button>
                    </Box>
                    <Grid templateColumns="repeat(7, 1fr)" gap={2} width="100%">
                        {['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'].map((day) => (
                            <Text key={day} color="tertiary" textAlign="center" fontSize="sm">
                                {day}
                            </Text>
                        ))}
                        {Array(firstDayOfMonth)
                            .fill('')
                            .map((_, index) => (
                                <Text key={`empty-${index}`} />
                            ))}
                        {daysInMonth.map((day) => {
                            const meetingsForDay = meetings.map((meeting) => {

                                const meetingDate = new Date(meeting.date);

                                if (meetingDate) {
                                    // console.log(meeting)
                                    if (
                                        meetingDate.getDate() === day &&
                                        meetingDate.getMonth() === displayedMonth.getMonth() &&
                                        meetingDate.getFullYear() === displayedMonth.getFullYear()
                                    ) {
                                        return meeting
                                    }
                                }

                            });
                            // console.log(meetingsForDay)
                            return (
                                <Box
                                    key={`day-${day}`}
                                    bg={day === currentDate.getDate() && displayedMonth.getMonth() === currentDate.getMonth() ? 'primary' : 'tertiary'}
                                    color={day === currentDate.getDate() && displayedMonth.getMonth() === currentDate.getMonth() ? 'opposite' : 'textPalette'}
                                    textAlign="center"
                                    fontSize={['sm', 'md', 'lg']}
                                    fontWeight={day === currentDate.getDate() && displayedMonth.getMonth() === currentDate.getMonth() ? 'bold' : 'normal'}
                                    p={2}
                                    rounded="md"
                                    height={`calc(100% - ${((100 / 7) * 0.07).toFixed(2)}%)`}
                                    position="relative"
                                    _hover={{
                                        cursor: 'pointer',
                                        opacity: 0.8,
                                        transform: 'scale(1.05)',
                                        boxShadow: '0 0 10px rgba(0, 0, 0, 0.2)',
                                    }}
                                    onClick={() => {
                                        setShowInvitationBox(true)

                                        setPickedDay(new Date(displayedMonth.getFullYear(), displayedMonth.getMonth(), day))
                                    }
                                    }
                                >
                                    <Text
                                        lineHeight="1.2"
                                        textAlign="center"
                                        fontFamily="cursive"
                                        fontStyle="italic"
                                    >
                                        {day}
                                    </Text>
                                    <Text lineHeight="1.2" fontSize="sm">
                                        {new Intl.DateTimeFormat('en-US', { weekday: 'long' }).format(new Date(displayedMonth.getFullYear(), displayedMonth.getMonth(), day))}
                                    </Text>
                                    {meetingsForDay.map((meeting, index) => (
                                        isParticipant(meeting) && (
                                            <Text key={index}>
                                                {meeting.title}
                                            </Text>
                                        )
                                    ))}
                                    <Box
                                        position="absolute"
                                        top="50%"
                                        left="50%"
                                        transform="translate(-50%, -50%)"
                                        opacity={0.4}
                                        fontSize={['4xl', '5xl', '6xl']}
                                        _hover={{
                                            opacity: 1,
                                        }}
                                        display="none"
                                    >
                                        +
                                    </Box>
                                    <Box
                                        position="absolute"
                                        top={0}
                                        left={0}
                                        width="100%"
                                        height="100%"
                                        display="flex"
                                        alignItems="center"
                                        justifyContent="center"
                                        opacity={0}
                                        _hover={{
                                            opacity: 1,
                                            background: 'rgba(255, 255, 255, 0.3)',
                                        }}
                                        transition="opacity 0.3s"
                                    >
                                        <Box
                                            fontSize={['4xl', '5xl', '6xl']}
                                            color="primary"
                                        >
                                            +
                                        </Box>
                                    </Box>
                                </Box>
                            )
                        }
                        )}
                    </Grid>
                    {showInvitationBox && (
                        <InvitationBox meetings={meetings} pickedDay={pickedDay} onClose={() => setShowInvitationBox(false)} onSubmit={handleInvitationSubmit} />
                    )}
                </Box>
            </VideoCallProvider>
        </ChakraProvider>
    );
};


export default Calendar;
