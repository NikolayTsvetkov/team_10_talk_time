import React, { useEffect, useState } from "react";
import { useAuth } from "../../../contexts/AuthContext";
import "./Chat.css";
import { Grid, Text } from "@chakra-ui/layout";
import ChatCard from "./ChatCard";
import { Button } from "@chakra-ui/button";
import CommonChat from "./CommonChat";
import { ChatNavbar } from "./ChatNavbar/ChatNavbar";
import { Messages } from "./Messages/Messages";
import Input from "./Input/Input";

const Chat = ({
  data,
  dataFromLeftSidebarChild,
  dataFromClickedLeftSidebarItem,
}) => {
  const { currentUser, currentUserFullInfo, usersByLevel } = useAuth();
  const [showChat, setShowChat] = useState(false);
  const [selectedChat, setSelectedChat] = useState("");

  useEffect(() => {
    setSelectedChat(dataFromClickedLeftSidebarItem.uid);
  }, [dataFromClickedLeftSidebarItem]);

  const handleToggleChat = () => {
    setShowChat(!showChat);
    if (usersByLevel?.length < 1) {
      return (
        <Text
          fontSize="xl"
          fontWeight="bold"
          color="purple.500"
          textAlign="center"
          mt={8}
        >
          No users in this category
        </Text>
      );
    }
  };

  const renderImage = () => {
    if (!data) {
      return (
          <img
            className="img-home"
            src="../../../../public/chat_alpha2.png"
            alt="Image"
          />
      );
    }
    return null;
  };

  const handleMessageIsEdited = (item) => {
   messageIsEdited(item)
  }
  return (
    <>
      {data === "Personal Messages" && (
        <div>
          <ChatNavbar
            currentUser={currentUser}
            currentUserFullInfo={currentUserFullInfo}
            dataFromClickedLeftSidebarItem={dataFromClickedLeftSidebarItem}
          />
          <Messages
            dataFromClickedLeftSidebarItem={dataFromClickedLeftSidebarItem}
            dataFromLeftSidebarChild={dataFromLeftSidebarChild}
            currentUser={currentUser}
          />
          <Input
            dataFromClickedLeftSidebarItem={dataFromClickedLeftSidebarItem}
            dataFromLeftSidebarChild={dataFromLeftSidebarChild}
            currentUser={currentUser}
          />
        </div>
      )}

      {data === "Levels" && (
        <>
          <Button
            onClick={handleToggleChat}
            colorScheme="purple"
            mt={1}
            alignSelf="top"
            px={8}
            mx={2}
          >
            {!showChat ? "Show Chat" : "Back to Users"}
          </Button>
          {!showChat ? (
            <Grid
              templateColumns="repeat(3, 1fr)"
              gap={12}
              justifyItems="center"
              paddingX={10}
            >
              {usersByLevel.map((user) => (
                <ChatCard
                  key={user.uid}
                  user={user}
                  currentUser={currentUser}
                />
              ))}
            </Grid>
          ) : (
            <CommonChat
              width="100%"
              users={usersByLevel}
              currentUser={currentUser}
            />
          )}
        </>
      )}
      {renderImage()}
    </>
  );
};

export default Chat;
