import {
    Box,
    Button,
    Text,
    Image,
    Circle,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    ModalCloseButton,
    useDisclosure,
    Textarea,
    Input,
    FormControl,
    FormErrorMessage,
  } from "@chakra-ui/react";
  import { createChat } from "../../../services/chat.service";
  import { useState } from "react";
  // import { useAuth } from "../../../contexts/AuthContext";
  
  const ChatCard = ({ user, currentUser }) => {
    const { firstName, lastName, level, uid } = user.fields;
    const status = currentUser?.uid === uid;
    const { isOpen, onOpen, onClose } = useDisclosure();
    const [content, setContent] = useState("");
    const [title, setTitle] = useState("");
    const [titleError, setTitleError] = useState("");
    const [contentError, setContentError] = useState("");
    // Placeholder image URL
    //TODO: Invite and Messages send invitation for chat from currentUser to choose user
    const imageUrl =
      user.imageUrl || "https://www.gravatar.com/avatar/?d=mp&s=150";
  
    // const { currentUser } = useAuth();
  
    const handleClickMessage = () => {
      createChat(title, content, currentUser?.uid, currentUser?.uid, user.uid);
      console.log("Message btn is clicked and message is sended");
      onClose()
    };
  
    const handleClick = () => {
      console.log("clicked send message");
      console.log("currentUser ", currentUser);
      console.log("user.uid ", user.uid);
      onOpen();
    };
  
    const handleTitleChange = (e) => {
      const newTitle = e.target.value;
      setTitle(newTitle);
  
      // Perform title validation
      if (newTitle.length < 5 || newTitle.length > 40) {
        setTitleError("Title must be between 5 and 40 characters.");
      } else {
        setTitleError("");
      }
    };
  
    const handleContentChange = (e) => {
      const newTitle = e.target.value;
      setContent(newTitle);
  
      // Perform title validation
      if (newTitle.length < 5 || newTitle.length > 40) {
        setContentError("Title must be between 5 and 40 characters.");
      } else {
        setContentError("");
      }
    };
  
    const handleClose = () => {
      setTitle("");
      setContent("");
      onClose();
    };
  
    return (
      <Box
        borderWidth="1px"
        borderRadius="lg"
        p={4}
        bg="gray.700"
        color="white"
        _hover={{ bg: "gray.600" }}
        transition="background-color 0.2s ease"
        textAlign="center"
        display="flex"
        flexDirection="column"
        justifyContent="center"
        alignItems="center"
        width="250px"
        height="300px"
        key={uid}
      >
        <Box mb={4}>
          <Image
            src={imageUrl}
            alt={`${firstName} ${lastName}`}
            borderRadius="full"
            boxSize="150px"
            objectFit="cover"
            fallbackSrc="https://via.placeholder.com/150"
          />
        </Box>
        <Text fontSize="lg" fontWeight="bold">
          {`${firstName} ${lastName}`}
        </Text>
        <Circle size="10px" bg={status === "online" ? "green.500" : "red.500"} />
        <Text>{level}</Text>
        <Box display="flex" justifyContent="space-between" mt={4}>
          <Button
            colorScheme="purple"
            size="sm"
            flex="1"
            mr={2}
            _focus={{ boxShadow: "none" }}
          >
            Invite
          </Button>
          <Button
            colorScheme="purple"
            size="sm"
            flex="1"
            ml={2}
            _focus={{ boxShadow: "none" }}
            onClick={handleClick}
          >
            Message
          </Button>
          <Modal
            isCentered
            onClose={onClose}
            isOpen={isOpen}
            motionPreset="slideInBottom"
          >
            <ModalOverlay />
            <ModalContent>
              <ModalHeader>Message to: {firstName} {lastName}</ModalHeader>
              <ModalCloseButton onClick={handleClose}/>
              <ModalBody>
                <FormControl isInvalid={titleError || contentError}>
                  <Input
                    placeholder="The title must be between 5 and 40 symbols."
                    value={title}
                    onChange={handleTitleChange}
                  />
                  {titleError && (
                    <FormErrorMessage>{titleError}</FormErrorMessage>
                  )}
                  <Textarea
                    placeholder="The content must be between 5 symbols and 100 symbols."
                    value={content}
                    onChange={handleContentChange}
                  />
                  {contentError && (
                    <FormErrorMessage>{contentError}</FormErrorMessage>
                  )}
                </FormControl>
              </ModalBody>
              <ModalFooter>
                <Button colorScheme="blue" mr={3} onClick={handleClose}>
                  Close
                </Button>
                <Button colorScheme="blue" mr={3} onClick={handleClickMessage}>
                  Send
                </Button>
              </ModalFooter>
            </ModalContent>
          </Modal>
        </Box>
      </Box>
    );
  };
  
  export default ChatCard;
  