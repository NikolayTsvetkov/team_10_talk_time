import React, { useEffect, useState, useRef } from "react";
import { Message } from "./Message";
import "./Messages.css";

export const Messages = ({
  dataFromClickedLeftSidebarItem,
  dataFromLeftSidebarChild,
  currentUser,
}) => {
  const [chat, setChat] = useState([]);
  const selectedChatUid = dataFromClickedLeftSidebarItem.uid;
  const userChats = dataFromLeftSidebarChild;
  const clickedItem = userChats && userChats.find((item) => item.uid === selectedChatUid);
  const selectedChat = clickedItem && clickedItem.conversation;
  const messagesEndRef = useRef(null);
  useEffect(() => {
    selectedChat ? setChat(selectedChat) : setChat([]);
  }, [dataFromLeftSidebarChild, dataFromClickedLeftSidebarItem]);

  useEffect(() => {
    if (chat) {
      scrollToBottom();
    }
  }, [chat]);

  const handleMessageClick = () => {
    console.log("click handleMessageClick");
  };

  const scrollToBottom = () => {
    messagesEndRef.current?.scrollIntoView({ behavior: "smooth" });
  };
  return (
    <div className="messages-container">
      <div className="messages-list">
        {currentUser &&
          chat &&
          chat.map((message, index) => (
            <div key={index} className="messages-list-chat">
              <Message
                isOwner={message?.author === currentUser?.uid}
                value={message}
                onClick={handleMessageClick}
                currentUser={currentUser}
                selectedChat={selectedChat}
                index={index}
              />
            </div>
          ))}
        <div ref={messagesEndRef} />
      </div>
    </div>
  );
};
