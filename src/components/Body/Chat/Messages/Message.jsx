import React, { useEffect, useState } from "react";
import { Avatar } from "@chakra-ui/react";
import "./Message.css";
import { getUserPhotoURL } from "../../../../services/users.service";
import EditMessageForm from "./EditMessageFrom";
import DeleteMessageForm from "./DeleteMessageFrom";

export const Message = ({ isOwner, value, index, currentUser }) => {
  const [showButtons, setShowButtons] = useState(false);
  const [authorPhotoURL, setAuthorPhotoURL] = useState("");
  const [isEditing, setIsEditing] = useState(false);
  const [isDelete, setIsDelete] = useState(false);

  const messageClass = !isOwner ? "message-owner" : "message";
  const messageColor = isOwner ? "#272849" : "#801414";

  useEffect(() => {
    const fetchAuthorPhotoURL = async () => {
      setAuthorPhotoURL(await getUserPhotoURL(value?.author));
    };

    fetchAuthorPhotoURL();
  }, [value?.author]);

  const handleOnClick = () => {
    if (value.author === currentUser.uid) {
      setShowButtons((prevShowButtons) => !prevShowButtons);
    }
  };

  const handleDeleteClick = async () => {
    setIsDelete(true);
  };

  const handleEditClick = () => {
    setIsEditing(true);
  };

  const handleFormClose = () => {
    setIsEditing(false);
  };

  return (
    value && (
      <div className={messageClass} onClick={handleOnClick}>
        <div className="message-info">
          <Avatar name={value?.author} src={authorPhotoURL} />
          <div className="time-span">
            <b>
              <span>{value?.createdOn}</span>
            </b>
          </div>
        </div>

        {isEditing ? (
          <EditMessageForm
            value={value}
            index={value.index}
            onClose={handleFormClose}
          />
        ) : (
          <div
            className="message-content"
            style={{ backgroundColor: messageColor }}
          >
            <p>{value?.content}</p>
          </div>
        )}

        {isDelete && (
          <DeleteMessageForm
            value={value.uid}
            index={index}
            onClose={handleFormClose}
          />
        )}

        {showButtons && (
          <div className="message-buttons">
            <button className="delete-button" onClick={handleDeleteClick}>
              delete
            </button>
            <button className="edit-button" onClick={handleEditClick}>
              edit
            </button>
          </div>
        )}
      </div>
    )
  );
};
