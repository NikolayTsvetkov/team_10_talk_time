import { useState, useRef } from "react";
import {
  FormControl,
  Input,
  Button,
  Flex,
  AlertDialog,
  AlertDialogOverlay,
  AlertDialogContent,
  AlertDialogHeader,
  AlertDialogBody,
  AlertDialogFooter,
} from "@chakra-ui/react";
import { updatePersonalMsg } from "../../../../services/chat.service";

const EditMessageForm = ({ value, onClose }) => {
  const [editedContent, setEditedContent] = useState(value.content);
  const [isDeleteAlertOpen, setIsDeleteAlertOpen] = useState(false);
  const cancelRef = useRef();

  const handleInputChange = (e) => {
    setEditedContent(e.target.value);
  };

  const handleEditClick = () => {
    setIsDeleteAlertOpen(true);
  };

  const handleEditConfirm = async () => {
    await updatePersonalMsg(value.uid, value.content, editedContent);
    onClose();
  };

  const handleCloseClick = () => {
    onClose();
  };

  return (
    <FormControl>
      <Input
        value={editedContent}
        onChange={handleInputChange}
        placeholder="Edit the message"
      />
      <Flex mt={2} justifyContent="flex-end">
        <Button colorScheme="teal" size="sm" mr={2} onClick={handleEditClick}>
          Save
        </Button>
        <Button size="sm" onClick={handleCloseClick}>
          Close
        </Button>
      </Flex>
      <AlertDialog
        isOpen={isDeleteAlertOpen}
        leastDestructiveRef={cancelRef}
        onClose={() => setIsDeleteAlertOpen(false)}
      >
        <AlertDialogOverlay>
          <AlertDialogContent>
            <AlertDialogHeader fontSize="lg" fontWeight="bold">
              Edit Message
            </AlertDialogHeader>
            <AlertDialogBody>
              Are you sure you want to edit this message with new value: "
              {editedContent}"?
            </AlertDialogBody>
            <AlertDialogFooter>
              <Button
                ref={cancelRef}
                onClick={() => setIsDeleteAlertOpen(false)}
              >
                No
              </Button>
              <Button colorScheme="blue" onClick={handleEditConfirm} ml={3}>
                Yes
              </Button>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </AlertDialog>
    </FormControl>
  );
};

export default EditMessageForm;
