import { useState, useRef } from "react";
import {
  FormControl,
  Button,
  AlertDialog,
  AlertDialogOverlay,
  AlertDialogContent,
  AlertDialogHeader,
  AlertDialogBody,
  AlertDialogFooter,
} from "@chakra-ui/react";
import { deletePersonalMsg } from "../../../../services/chat.service";

const DeleteMessageForm = ({ value, index, onClose }) => {
  const [isDeleteAlertOpen, setIsDeleteAlertOpen] = useState(true);
  const cancelRef = useRef();

  const handleDeleteConfirm = async () => {
    await deletePersonalMsg(value, index);
    onClose();
  };

  const handleCloseClick = () => {
    setIsDeleteAlertOpen(false);
    onClose();
  };

  return (
    <FormControl>
      <AlertDialog
        isOpen={isDeleteAlertOpen}
        leastDestructiveRef={cancelRef}
        onClose={handleCloseClick}
      >
        <AlertDialogOverlay>
          <AlertDialogContent>
            <AlertDialogHeader fontSize="lg" fontWeight="bold">
              Delete Message
            </AlertDialogHeader>
            <AlertDialogBody>
              Are you sure you want to delete this message?
            </AlertDialogBody>
            <AlertDialogFooter>
              <Button ref={cancelRef} onClick={handleCloseClick}>
                No
              </Button>
              <Button colorScheme="red" onClick={handleDeleteConfirm} ml={3}>
                Yes
              </Button>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </AlertDialog>
    </FormControl>
  );
};

export default DeleteMessageForm;