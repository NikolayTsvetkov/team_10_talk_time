import React, { useState, useEffect } from "react";
import { getAllUsers } from "../../../../services/users.service";
import { createTalkGroup } from "../../../../services/chat.service";

const CreateTalkGroupModal = ({ onClose, currentUser }) => {
  const [users, setUsers] = useState([]);
  const [selectedUsers, setSelectedUsers] = useState([]);
  const [searchedMessages, setSearchedMessages] = useState([]);

  useEffect(() => {
    const fetchAllUsers = async () => {
      const users = await getAllUsers();
      setUsers(users);
      console.log(users);
    };

    fetchAllUsers();
  }, []);

  const handleUserSelection = (user) => {
    const isSelected = selectedUsers.includes(user);
    if (isSelected) {
      setSelectedUsers(
        selectedUsers.filter((selectedUser) => selectedUser !== user)
      );
    } else {
      setSelectedUsers([...selectedUsers, user]);
    }
  };
  const handleCreateTalkGroup = () => {
    const selectedUsernames = selectedUsers.map(
      (user) => user?.fields?.username
    );
    const selectedUsersUid = selectedUsers.map((user) => user?.uid);
    const title = currentUser?.username;
    const content = `Group Chat: ${selectedUsernames.join(", ")}`;
    const handle = currentUser?.uid;
    const participants = [...selectedUsersUid, handle];

    createTalkGroup(title, content, handle, participants);
    onClose();
  };

  // const usersToIterate = searchedMessages.length > 0 ? searchedMessages : users;

  return (
    <div>
      <button onClick={handleCreateTalkGroup}>
        <b>Create Talk Group</b>
      </button>
      <button onClick={onClose}>
        <b>Cancel</b>
      </button>
      {/* <div>
        <LeftSidebarSearch
          messages={users}
          setSearchedMessages={setSearchedMessages}
        />
      </div> */}
      <ul>
        {users.map((user, i) => (
          <li
            key={i}
            onClick={() => handleUserSelection(user)}
            style={{
              backgroundColor: selectedUsers.includes(user)
                ? "#27284966"
                : "rgb(208, 208, 208",
              cursor: "pointer",
            }}
          >
            {`${user?.fields?.username} - ${user?.fields?.level} `}
          </li>
        ))}
      </ul>
    </div>
  );
};

export default CreateTalkGroupModal;

// import React, { useState, useEffect } from "react";
// import { fetchUsers } from "../../../../services/users.service";
// import { LeftSidebarSearch } from "../../LeftSideBarChild/LeftSidebarSearch/LeftSidebarSearch";

// const CreateTalkGroupModal = ({ onClose }) => {
//   const [users, setUsers] = useState([]);
//   const [selectedUsers, setSelectedUsers] = useState([]);
//   const [searchedMessages, setSearchedMessages] = useState([]);

//   useEffect(() => {
//     const fetchAllUsers = async () => {
//       const users = await fetchUsers();
//       setUsers(users);
//     };

//     fetchAllUsers();
//   }, []);

//   const handleUserSelection = (user) => {
//     const isSelected = selectedUsers.includes(user);
//     if (isSelected) {
//       setSelectedUsers(
//         selectedUsers.filter((selectedUser) => selectedUser !== user)
//       );
//     } else {
//       setSelectedUsers([...selectedUsers, user]);
//     }
//   };

//   const handleCreateTalkGroup = () => {
//     // Perform the logic to create a talk group with the selected users
//     // You can use the selectedUsers array to get the users to be added to the conversation
//     // Example: const talkGroupData = { name: "Talk Group Name", users: selectedUsers }
//     // createTalkGroup(talkGroupData);

//     // Close the modal after creating the talk group
//     onClose();
//   };
//   const usersToIterate = searchedMessages | users
//   return (
//     <div>
//       <div>
//         <LeftSidebarSearch messages={users} setSearchedMessages={setSearchedMessages}/>
//       </div>
//       <ul>
//         {usersToIterate.map(
//           (user, i) => (
//             console.log(user),
//             (
//               <li
//                 key={i}
//                 onClick={() => handleUserSelection(user)}
//                 style={{
//                   backgroundColor: selectedUsers.includes(user)
//                     ? "lightblue"
//                     : "white",
//                   cursor: "pointer",
//                 }}
//               >
//                 {user.username}
//               </li>
//             )
//           )
//         )}
//       </ul>

//       <button onClick={handleCreateTalkGroup}>Create Talk Group</button>
//       <button onClick={onClose}>Cancel</button>
//     </div>
//   );
// };

// export default CreateTalkGroupModal;

// // import React, { useState, useEffect } from "react";
// // import { fetchUsers } from "../../../services/user.service";

// // const CreateTalkGroupModal = ({ onClose }) => {
// //   const [users, setUsers] = useState([]);
// //   const [selectedUsers, setSelectedUsers] = useState([]);

// //   useEffect(() => {
// //     const fetchAllUsers = async () => {
// //       const users = await fetchUsers();
// //       setUsers(users);
// //     };

// //     fetchAllUsers();
// //   }, []);

// //   const handleUserSelection = (user) => {
// //     const isSelected = selectedUsers.includes(user);
// //     if (isSelected) {
// //       setSelectedUsers(
// //         selectedUsers.filter((selectedUser) => selectedUser !== user)
// //       );
// //     } else {
// //       setSelectedUsers([...selectedUsers, user]);
// //     }
// //   };

// //   const handleCreateTalkGroup = () => {
// //     // Perform the logic to create a talk group with the selected users
// //     // You can use the selectedUsers array to get the users to be added to the conversation
// //     // Example: const talkGroupData = { name: "Talk Group Name", users: selectedUsers }
// //     // createTalkGroup(talkGroupData);

// //     // Close the modal after creating the talk group
// //     onClose();
// //   };

// //   return (
// //     <div>
// //       <ul>
// //         {users.map((user) => (
// //           <li
// //             key={user.uid}
// //             onClick={() => handleUserSelection(user)}
// //             style={{
// //               backgroundColor: selectedUsers.includes(user)
// //                 ? "lightblue"
// //                 : "white",
// //             }}
// //           >
// //             {user.name}
// //           </li>
// //         ))}
// //       </ul>

// //       <button onClick={handleCreateTalkGroup}>Create Talk Group</button>
// //       <button onClick={onClose}>Cancel</button>
// //     </div>
// //   );
// // };

// // export default CreateTalkGroupModal;
