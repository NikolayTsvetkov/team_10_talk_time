import React, { useState } from "react";
import {
  Box,
  Flex,
  Text,
  Input,
  Button,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Textarea,
  FormControl,
  FormErrorMessage,
  Modal,
  useDisclosure,
  ModalOverlay,
} from "@chakra-ui/react";
import { addChatMessage } from "../../../services/level.services";
import { useEffect } from "react";
import { off, onValue, ref } from "firebase/database";
import { db } from "../../../config/firebase-config";
import { useAuth } from "../../../contexts/AuthContext";

const CommonChat = ({ users, currentUser }) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [message, setMessage] = useState("");
  const [chatMessages, setChatMessages] = useState([]);
  const [titleError, setTitleError] = useState("");
  const [contentError, setContentError] = useState("");
  const [title, setTitle] = useState("");
  const [content, setContent] = useState("");

  const { level } = useAuth();
  // console.log(level)

  useEffect(() => {
    console.log(level);
    const chatRef = ref(db, `chatByUsersLevel/${level}`);

    const callback = (snapshot) => {
      const data = snapshot.val();
      const messages = data ? Object.values(data) : [];
      console.log(messages);
      setChatMessages(messages);
    };

    onValue(chatRef, callback);

    return () => {
      off(chatRef, callback);
    };
  }, [level]);

  const handleSendMessage = () => {
    // Handle sending the message
    const newMessage = {
      user: currentUser?.username || 2,
      message,
      date: new Date().toISOString(),
    };
    addChatMessage(newMessage, level);
    // Save the new message to the Realtime Database

    setChatMessages([...chatMessages, newMessage]);
    setMessage("");
  };

  const handleTitleChange = (e) => {
    const newTitle = e.target.value;
    setTitle(newTitle);

    if (newTitle.length < 5 || newTitle.length > 40) {
      setTitleError("Title must be between 5 and 40 characters.");
    } else {
      setTitleError("");
    }
  };

  const handleContentChange = (e) => {
    const newTitle = e.target.value;
    setContent(newTitle);

    if (newTitle.length < 5 || newTitle.length > 40) {
      setContentError("Title must be between 5 and 40 characters.");
    } else {
      setContentError("");
    }
  };

  const handleClose = () => {
    setTitle("");
    setContent("");
    onClose();
  };

  const handleClickMessage = () => {
    console.log('users ', users);
    // createChat(title, content, currentUser?.uid, currentUser?.uid, users.uid);
    console.log("Message btn is clicked and message is sended");
    onClose();
  };

  // console.log(chatMessages)
  return (
    <Box bg="gray.800" color="white" borderRadius="md" p={4} width="100%">
      <Flex height="100%">
        <Box width="25%" pr={4}>
          <Text fontWeight="bold" mb={2}>
            Users
          </Text>
          {users.map((user) => (
            <Text key={user?.uid} mb={2}>
              {user?.fields?.firstName} {user?.fields?.lastName}
            </Text>
          ))}
        </Box>
        <Box width="75%" flex={1}>
          <Box
            height="75vh"
            bgGradient="linear(to-b, purple.800, purple.500)"
            borderRadius="md"
            p={4}
            overflowY="scroll"
            style={{ display: "flex" }}
            flexDirection="column"
          >
            {chatMessages &&
              chatMessages.map((chat, id) => (
                <Box
                  key={chat.date}
                  bgGradient={
                    chat.user === currentUser?.username
                      ? "linear(to-b, purple.800, black)"
                      : "linear(to-b, purple.500, purple.100)"
                  }
                  borderRadius="md"
                  p={2}
                  mb={2}
                  alignSelf={
                    chat.user === currentUser?.username
                      ? "flex-end"
                      : "flex-start"
                  }
                  flexDirection={
                    chat.user === currentUser?.username ? "row-reverse" : "row"
                  }
                  width="75%"
                >
                  <Text
                    color={
                      chat.user === currentUser?.username ? "white" : "black"
                    }
                    flex="1"
                  >
                    {chat.message}
                  </Text>
                  <Text
                    color={
                      chat.user === currentUser?.username ? "white" : "gray.800"
                    }
                    fontSize="sm"
                    textAlign="right"
                  >
                    {chat.user}
                  </Text>
                  <Text
                    color={
                      chat.user === currentUser?.username ? "white" : "gray.800"
                    }
                    fontSize="sm"
                    textAlign="right"
                  >
                    {new Date(chat.date).toLocaleString()}
                  </Text>
                </Box>
              ))}
          </Box>
          <Flex mt={4}>
            <Input
              value={message}
              onChange={(e) => setMessage(e.target.value)}
              onKeyDown={(e) => {
                if (e.key === "ArrowUp") {
                  e.preventDefault();
                  handleEditLastMessage();
                }
              }}
              placeholder="Enter your message..."
              flex="1"
              mr={2}
            />
            <Button onClick={handleSendMessage} colorScheme="purple">
              Send
            </Button>
            <Modal
              isCentered
              onClose={onClose}
              isOpen={isOpen}
              motionPreset="slideInBottom"
            >
              <ModalOverlay />
              <ModalContent>
                <ModalHeader>
                  Message to: {currentUser?.firstName} {currentUser?.lastName}
                </ModalHeader>
                <ModalCloseButton onClick={handleClose} />
                <ModalBody>
                  <FormControl isInvalid={titleError || contentError}>
                    <Input
                      placeholder="The title must be between 5 and 40 symbols."
                      value={title}
                      onChange={handleTitleChange}
                    />
                    {titleError && (
                      <FormErrorMessage>{titleError}</FormErrorMessage>
                    )}
                    <Textarea
                      placeholder="The content must be between 5 symbols and 100 symbols."
                      value={content}
                      onChange={handleContentChange}
                    />
                    {contentError && (
                      <FormErrorMessage>{contentError}</FormErrorMessage>
                    )}
                  </FormControl>
                </ModalBody>
                <ModalFooter>
                  <Button colorScheme="blue" mr={3} onClick={handleClose}>
                    Close
                  </Button>
                  <Button
                    colorScheme="blue"
                    mr={3}
                    onClick={handleClickMessage}
                  >
                    Send
                  </Button>
                </ModalFooter>
              </ModalContent>
            </Modal>
          </Flex>
        </Box>
      </Flex>
    </Box>
  );
};

export default CommonChat;
