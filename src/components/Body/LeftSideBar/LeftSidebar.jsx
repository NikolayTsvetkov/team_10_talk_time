import React, { useEffect, useState } from "react";
import "./LeftSideBar.css";
import {
  sidebarDataBottom,
  sidebarDataTop,
} from "../../../testData/sideBarTestData";
import {
  LeftSidebarBottomIcons,
  LeftSidebarTopIcons,
} from "./LeftSidebarLoops";
import "./LeftSidebar.css";
import { useNavigate } from "react-router-dom";

const SideBar = ({ sendDataToParent, sendBooleanToParent }) => {
  const topIcons = sidebarDataTop;
  const BottomIcons = sidebarDataBottom;
  const [activeIcon, setActiveIcon] = useState(null);
  const [deactivateIcon, setDeactivateIcon] = useState(1);
  const [close, setClose] = useState(false);
  const [id, setID] = useState('');
  const navigate = useNavigate()


  const handleIconOpenClose = (item) => {

    setActiveIcon(item.id);
    sendDataToParent(item.name);
    setDeactivateIcon(deactivateIcon + 1);
    setID(item.id)

    if (activeIcon === item.id && close) {
      sendBooleanToParent(false)
      setClose(false)
    } else {
      sendBooleanToParent(true)
      setClose(true)
    }
    if (item?.link) {
      navigate(item?.link)
    }

  };

  return (
    <div className="sidebar-left">
      <div className="sidebar-left-top">
        {topIcons.map((item, index) => (
          <LeftSidebarTopIcons
            key={index}
            item={item}
            isActive={item.id === activeIcon}
            handleClick={handleIconOpenClose}
          />
        ))}
      </div>

      <div className="sidebar-left-bottom">
        {BottomIcons.map((item, index) => (
          <LeftSidebarBottomIcons
            key={index}
            item={item}
            isActive={item.id === activeIcon}
            handleClick={handleIconOpenClose}
          />
        ))}
      </div>
    </div>
  );
};

export default SideBar;