import React from "react";
import "./LeftSidebarChild.css";
import { useEffect, useState } from "react";
import {
  LeftSidebarChildLoopDM,
  LeftSidebarChildLoop,
  LeftSidebarChildLoopGroupTalk,
} from "./LeftSidebarChildLoops";
import { fetchUsersByLevel } from "../../../services/level.services";
import { useAuth } from "../../../contexts/AuthContext";
import { LeftSidebarSearch } from "./LeftSidebarSearch/LeftSidebarSearch";
import {
  changesListenerUsersChat,
  fetchAllPMByUser,
  fetchAllTalkGroup,
} from "../../../services/chat.service";
import { leftSidebarChildData } from "../../../testData/sideBarTestData";
import CreateTalkGroupModal from "../Chat/TalkGroups/CreateTalkGroupModal";

const LeftSidebarChild = ({
  data,
  sendMessagesToParent,
  sendClickedItemToParent,
}) => {
  const { currentUser, handleSetUsersByLevel, userPersonalMessages, setLevel } =
    useAuth();
  const [messages, setMessages] = useState([]);
  const [talkGroups, setTalkGroups] = useState([]);
  const [clickedItemId, setClickedItemId] = useState(null);
  const [unreadCount, setUnreadCount] = useState(0);
  const [searchedMessages, setSearchedMessages] = useState([]);
  const [isCreateGroupModalOpen, setIsCreateGroupModalOpen] = useState(false);

  const handleUsersByLevel = async (level) => {
    const usersByLevel = await fetchUsersByLevel(level);
    setLevel(level);
    handleSetUsersByLevel(usersByLevel);
  };

  const handleUsersByPM = async (personalMessages) => {
    setMessages(
      await fetchAllPMByUser(Object.keys(personalMessages)?.reverse())
    );
  };

  useEffect(() => {
    const getAllTalkGroup = async () => {
      setTalkGroups(await fetchAllTalkGroup());
    };
    getAllTalkGroup();
  }, []);

  useEffect(() => {
    if (userPersonalMessages) {
      const setMessageCallback = () => {
        handleUsersByPM(userPersonalMessages);
      };
      const unsubscribe = changesListenerUsersChat(setMessageCallback);
      return () => unsubscribe();
    }
  }, [userPersonalMessages]);

  useEffect(() => {
    if (messages) {
      sendMessagesToParent(messages);

      const count = messages.reduce((acc, item) => {
        if (!item.read[currentUser.uid]) {
          return acc + 1;
        }
        return acc;
      }, 0);
      setUnreadCount(count);
    }
  }, [messages]);

  const handleChildData = () => {
    setMessages(messages);
  };

  const handleClickedItem = (item) => {
    sendClickedItemToParent(item);
    setClickedItemId(item);
  };

  const unreadMsgCheck = (count) => {
    return (
      <label>
        {count > 0 &&
          (count === 1 ? (
            <b>
              <p>{count} unread message</p>
            </b>
          ) : (
            <b>
              <p>{count} unread messages</p>
            </b>
          ))}
      </label>
    );
  };
  return (
    <div className="left-sidebar-child">
      {data === "Personal Messages" && currentUser && (
        <div className="left-sidebar-child-conversation">
          <label>Messages</label>
          {unreadMsgCheck(unreadCount)}

          <LeftSidebarSearch
            messages={messages}
            setSearchedMessages={setSearchedMessages}
          />
          <div className="left-sidebar-child">
            {searchedMessages.length > 0
              ? searchedMessages.map((item, index) => (
                  <LeftSidebarChildLoopDM
                    key={index}
                    item={item}
                    isActive={clickedItemId && item.uid === clickedItemId.uid}
                    currentUser={currentUser}
                    sendMessagesToParent={handleChildData}
                    sendClickedItemToParent={handleClickedItem}
                  />
                ))
              : messages.map((item, index) => (
                  <LeftSidebarChildLoopDM
                    key={index}
                    item={item}
                    isActive={clickedItemId && item.uid === clickedItemId.uid}
                    currentUser={currentUser}
                    sendMessagesToParent={handleChildData}
                    sendClickedItemToParent={handleClickedItem}
                  />
                ))}
          </div>
        </div>
      )}
      {data === "Levels" && (
        <div>
          {" "}
          <label>Levels</label>
          <div className="left-sidebar-child-info">
            {leftSidebarChildData.map((item, index) => (
              <LeftSidebarChildLoop
                handleClick={() => handleUsersByLevel(item?.level)}
                key={index}
                item={item}
                isActive={item?.uid === clickedItemId?.uid}
              />
            ))}
          </div>
        </div>
      )}
      {data === "Talk groups" && (
        <div className="talk-group">
          <div>
            <label>Talk Groups</label>
            <div className="left-sidebar-child-info">
              <button onClick={() => setIsCreateGroupModalOpen(true)}>
                <b>Show all users</b>
              </button>
            </div>
          </div>
          {isCreateGroupModalOpen && (
            <CreateTalkGroupModal
              onClose={() => setIsCreateGroupModalOpen(false)}
              currentUser={currentUser}
              // onTalkGroupCreated={setCreatedTalkGroup}
            />
          )}
          {console.log(Object.values(talkGroups))}
          {talkGroups.length > 0 &&
            talkGroups.map(
              (item, index) => console.log(item)

              // <LeftSidebarChildLoopDM
              //   key={index}
              //   item={item}
              //   currentUser={currentUser}
              // />
            )}
          {Object.values(talkGroups).map((item, index) => (
            <LeftSidebarChildLoopGroupTalk
              key={index}
              item={item}
              currentUser={currentUser}
              sendMessagesToParent={handleChildData}
              sendClickedItemToParent={handleClickedItem}
            />
          ))}
        </div>
      )}
    </div>
  );
};

export default LeftSidebarChild;
