import "./LeftSidebarChildLoops.css";
import { Avatar, Button, Flex, Icon, IconButton } from "@chakra-ui/react";
import textLengthChecker from "../../../checkFunctions/textLengthChecker";
import { getUserByUid } from "../../../services/users.service";
import { useEffect, useState } from "react";
import {
  findAnotherParticipant,
  updateMessageReadStatus,
} from "../../../services/chat.service";

export const LeftSidebarChildLoop = ({ item, isActive, handleClick }) => {
  const classList = isActive
    ? "sidebar-child-loop active"
    : "sidebar-child-loop";

  return (
    <div>
      <Button
        key={item.id}
        bg="white"
        className={classList}
        variant="ghost"
        size="md"
        onClick={handleClick}
        _hover={{ backgroundColor: "gray.200" }}
        _focus={{ outline: "none" }}
      >
        {item.level && (
          <label className="sidebar-child-label">{item.level}</label>
        )}
      </Button>
    </div>
  );
};

export const LeftSidebarChildLoopDM = ({
  item,
  isActive,
  currentUser,
  sendMessagesToParent,
  sendClickedItemToParent,
}) => {
  const classList = isActive
    ? "sidebar-child-loop active"
    : "sidebar-child-loop";
  const [otherUserInfo, setOtherUserInfo] = useState(null);
  const [newMessage, setNewMessage] = useState(false);

  useEffect(() => {
    const waitingFunc = async () => {
      const responseUID = await findAnotherParticipant(item, currentUser);
      const responseUser = await getUserByUid(responseUID);
      setOtherUserInfo(responseUser);
    };
    waitingFunc();
    if (item.read[currentUser.uid] === false) {
      setNewMessage(true);
    }
  }, [item, currentUser]);

  const handleClick = async (onClick) => {
    sendMessagesToParent(onClick);
    sendClickedItemToParent(item);

    if (item.read[currentUser.uid] === false) {
      updateMessageReadStatus(item.uid, true, currentUser.uid);
      setNewMessage(false);
    }
  };

  return (
    <button onClick={handleClick} className={classList}>
      <div className="btn-left-sidebar-child-loop">
        <div className="sidebar-child-icon">
          <Avatar
            name={otherUserInfo?.firstName}
            src={otherUserInfo?.photoURL}
          />
        </div>

        <div className="sidebar-child-theme-conversation">
          {item?.content && (
            <p>
              <label className="sidebar-child-label-conversation">
                {textLengthChecker(item?.content)}
              </label>
            </p>
          )}
          {item?.title && (
            <h2 className="sidebar-child-label">
              {newMessage && <p className="unread-message">New message</p>}
              {textLengthChecker(item?.title)}
            </h2>
          )}
        </div>
      </div>
    </button>
  );
};


export const LeftSidebarChildLoopGroupTalk = ({
  item,
  isActive,
  currentUser,
  sendMessagesToParent,
  sendClickedItemToParent,
}) => {
  const classList = isActive
    ? "sidebar-child-loop active"
    : "sidebar-child-loop";
  const [otherUserInfos, setOtherUserInfos] = useState([]);
  const [newMessage, setNewMessage] = useState(false);

  useEffect(() => {
    const waitingFunc = async () => {
      const participantsUids = item.participants.filter(uid => uid !== currentUser.uid);
      const responseUsers = await Promise.all(participantsUids.map(uid => getUserByUid(uid)));
      setOtherUserInfos(responseUsers);
    };
    waitingFunc();
    if (item.read[currentUser.uid] === false) {
      setNewMessage(true);
    }
  }, [item, currentUser]);

  const handleClick = async (onClick) => {
    sendMessagesToParent(onClick);
    sendClickedItemToParent(item);

    if (item.read[currentUser.uid] === false) {
      updateMessageReadStatus(item.uid, true, currentUser.uid);
      setNewMessage(false);
    }
  };

  return (
    <button onClick={handleClick} className={classList}>
      <div className="btn-left-sidebar-child-loop">
        <div className="sidebar-child-icon">
          {otherUserInfos.map((userInfo, index) => (
            <Avatar key={index} name={userInfo?.firstName} src={userInfo?.photoURL} />
          ))}
        </div>

        <div className="sidebar-child-theme-conversation">
          {item?.content && (
            <p>
              <label className="sidebar-child-label-conversation">
                {textLengthChecker(item?.content)}
              </label>
            </p>
          )}
          {item?.title && (
            <h2 className="sidebar-child-label">
              {newMessage && <p className="unread-message">New message</p>}
              {textLengthChecker(item?.title)}
            </h2>
          )}
        </div>
      </div>
    </button>
  );
};
