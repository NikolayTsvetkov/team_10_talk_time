import React, { useState } from "react";
import "./LeftSidebarSearch.css";
import { searchUserByName } from "../../../../services/users.service";

export const LeftSidebarSearch = ({ messages, setSearchedMessages }) => {
  const [username, setUsername] = useState("");
  const [users, setUsers] = useState([]);

  const handleSearch = async (name) => {
    const searchResults = await searchUserByName(name);
    console.log("searchResults ", searchResults);
    setUsers(searchResults);
  };

  const handleChange = (e) => {
    const name = e.target.value;
    setUsername(name);
  
    const searchResults = messages.filter((msg) =>
      msg.title.toLowerCase().includes(name.toLowerCase())
    );
    setSearchedMessages(searchResults);
  };

  return (
    <div className="search-form">
      <input
        className="search-form-input"
        type="text"
        placeholder="Find a user"
        value={username}
        onChange={handleChange}
      />
      {users.length > 0 && (
        <ul>
          {users.map((user) => (
            <li key={user.uid}>{user.name}</li>
          ))}
        </ul>
      )}
    </div>
  );
};
