import React, { useEffect, useState } from "react";
import Chat from "./Chat/Chat";
import LeftSidebarChild from "./LeftSideBarChild/LeftSidebarChild";
import LeftSideBar from "./LeftSideBar/LeftSidebar";
import "./Body.css";
import Calendar from "../Calendar/Calendar";
import { Route, Routes } from "react-router-dom";
import UsersGridByLevel from "../Levels/UsersGridByLevel";

const Body = () => {
  const [data, setData] = useState("");
  const [dataFromLeftSidebarChild, setDataFromLeftSidebarChild] = useState("");
  const [dataFromClickedLeftSidebarItem, setDataClickedItem] = useState({});
  const [isLeftSidebarChildVisible, setIsLeftSidebarChildVisible] =
    useState(true);

  const handleChildDataLeftSideBar = (childData) => {
    setData(childData);
    setIsLeftSidebarChildVisible(!isLeftSidebarChildVisible);
  };

  const handleChildBooleanLeftSideBar = (childBoolean) => {
    if (!childBoolean) setData("");
  };

  const handleChildLeftSidebarChild = (childNewData) => {
    setDataFromLeftSidebarChild(childNewData);
  };

  const handleClickedItem = (childNewData) => {
    setDataClickedItem(childNewData);
  };

  return (
    <div className="chat-container">
      <div className="chat-body">
        <div className="chat-sidebar">
          <LeftSideBar
            sendDataToParent={handleChildDataLeftSideBar}
            sendBooleanToParent={handleChildBooleanLeftSideBar}
          />
        </div>
        <div className="chat-LeftSidebarChild">
          <LeftSidebarChild
            data={data}
            sendMessagesToParent={handleChildLeftSidebarChild}
            sendClickedItemToParent={handleClickedItem}
          />{" "}
        </div>
        <div className="chat-msg">
          <Routes>
            <Route
              path="/chat"
              element={
                <Chat
                  data={data}
                  dataFromLeftSidebarChild={dataFromLeftSidebarChild}
                  dataFromClickedLeftSidebarItem={
                    dataFromClickedLeftSidebarItem
                  }
                />
              }
            />
            {/* <Route path="/" element={<Landing />} /> */}
            <Route path="/calendar" element={<Calendar />} />
            <Route path="/levels" element={<UsersGridByLevel />} />
          </Routes>
        </div>
      </div>
    </div>
  );
};

export default Body;
