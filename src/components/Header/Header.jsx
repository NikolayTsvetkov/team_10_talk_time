import React from "react";
import "./Header.css";
import Navbar from "./Navbar/Navbar";
import { useAuth } from "../../contexts/AuthContext";
import { Button } from "@chakra-ui/react";
import UserMenu from "./UserMenu/UserMenu";
import { Link } from "react-router-dom";
import logoImage from "../../../public/TalkTime-logo1.png";
import { getOnlineUsers } from "../../services/users.service";
import { createChat } from "../../services/chat.service";

const Header = () => {
  const { currentUser, currentUserFullInfo } = useAuth();

  const chooseRandomOnlineUserIndex = (userArrayLength) => {
    return Math.floor(Math.random() * userArrayLength);
  };

  const handleStartAQuickChat = async () => {
    const allOnlineUsers = await getOnlineUsers();
    const potentialUsersForChat = allOnlineUsers.filter(
      (user) => user.fields.email !== currentUserFullInfo.email
    );
    const randomUserIndex = chooseRandomOnlineUserIndex(
      potentialUsersForChat.length
    );
    const randomUser = potentialUsersForChat[randomUserIndex];
    console.log(randomUser);

    if (randomUser) createChat(randomUser.fields.username, "Quick talk!", currentUser.uid, currentUser.uid, randomUser.uid);
    
  };

  return (
    <>
      <div className="header">
        <Link to="/">
          <img className="header-logo" src={logoImage} alt="TalkTime Logo" />
        </Link>
        {currentUserFullInfo?.status === "online" && (
          <div className="quick-talk-button">
            {currentUserFullInfo?.status === "online" && (
              <div className="quick-talk-button">
                <Button
                  variant="solid"
                  color="green"
                  onClick={handleStartAQuickChat}
                >
                  Quick Talk
                </Button>
              </div>
            )}
          </div>
        )}
        <div className="header-userInfo">
          {currentUser && <UserMenu />}
          {!currentUser && <Navbar />}
        </div>
      </div>
    </>
  );
};

export default Header;
