
import { Button, Modal, ModalOverlay, ModalContent, ModalHeader, ModalBody, ModalFooter, ModalCloseButton, Stack, Box, FormLabel, Input, Select, Textarea, useDisclosure, FormControl } from "@chakra-ui/react"
import { useNavigate } from "react-router-dom/dist";
import { useState } from 'react';
import { useAuth } from '../../../contexts/AuthContext';
import { registerUser } from '../../../services/auth.service';
import { useValidation } from '../../../contexts/ValidationCOntext';


const inputFields = {
    username: '',
    password: '',
    firstName: '',
    lastName: '',
    email: '',
    level: '',
    about: ''
};

export default function SignUpModal() {
    const { isOpen, onOpen, onClose } = useDisclosure()
    const navigate = useNavigate();
    //fix imports and logic
    const [fields, setFields] = useState(inputFields);
    const { username, password, firstName, lastName, email, level, about } = fields;
    const { handleSetCurrentUser } = useAuth();
    const { errors, addError, clearErrors } = useValidation();

    const handleChange = (event) => {
        const { name, value } = event.target;
        setFields({ ...fields, [name]: value });
        console.log(fields)
    };

    const registerUsers = async (e) => {
        e.preventDefault();
        console.log(e)

        if (username.length < 5 || username.length > 35) {
            addError('Username should be between 5 and 35 symbols');
            return;
        }

        try {
            const user = await registerUser(fields);
            console.log(user, 'the user from the form')
            handleSetCurrentUser(user);
            navigate('/');
        }
        catch (error) {
            console.log('SignUp error:', Object.keys(error), error.code, error.name, error.customData);

            addError(error.code)
        }
    };
    //end 
    return (
        <>
            <form onSubmit={registerUsers}>
                <Modal
                    closeOnOverlayClick={false}
                    isCentered
                    onClose={onClose}
                    isOpen={true}
                    motionPreset='slideInBottom'
                >
                    <ModalOverlay />
                    <ModalContent>
                        <ModalHeader>Create a new user</ModalHeader>
                        <ModalCloseButton onClick={() => navigate('/')} />
                        <ModalBody>
                            <Stack spacing='24px'>
                                <FormControl isRequired>
                                    <Box>
                                        <FormLabel>Username</FormLabel>
                                        <Input
                                            onChange={handleChange}
                                            name="username"
                                            value={username}
                                        />
                                    </Box>
                                </FormControl>
                                <Box>
                                    <FormControl isRequired>
                                        <FormLabel>Password</FormLabel>
                                        <Input
                                            onChange={handleChange}
                                            name="password"
                                            type={'password'}
                                            value={password}
                                        />
                                    </FormControl>
                                </Box>
                                <Box>
                                    <FormControl isRequired>
                                        <FormLabel>E-mail</FormLabel>
                                        <Input
                                            onChange={handleChange}
                                            type="email"
                                            name="email"
                                            value={email}
                                        />
                                    </FormControl>
                                </Box>
                                <Box>
                                    {/* <FormControl isRequired> */}
                                    <FormLabel>First Name</FormLabel>
                                    <Input
                                        onChange={handleChange}
                                        name="firstName"
                                        value={firstName}
                                    />
                                    {/* </FormControl> */}
                                </Box>
                                <Box>
                                    <FormLabel>Last Name</FormLabel>
                                    <Input
                                        onChange={handleChange}
                                        name="lastName"
                                        value={lastName}
                                    />
                                </Box>


                                <Box>

                                    <FormLabel>Select Level</FormLabel>
                                    <FormControl isRequired>
                                        <Select id='owner'
                                            defaultValue="A1"
                                            name="level"
                                            onChange={handleChange}
                                        >
                                            <option value="A1">Beginner(A1)</option>
                                            <option value="A2">Elementary(A2)</option>
                                            <option value="B1">Intermediate(B1)</option>
                                            <option value="B2">Upper-intermediate(B2)</option>
                                            <option value="C1">Advanced(C1)</option>
                                            <option value="C2">Mastery(C2)</option>
                                        </Select>
                                    </FormControl>

                                </Box>

                                <Box>
                                    <FormLabel htmlFor='desc'>About</FormLabel>
                                    <Textarea id='about'
                                        name="about"
                                        onChange={handleChange}
                                    />
                                </Box>
                            </Stack>
                        </ModalBody>
                        <ModalFooter>
                            <Button variant='ghost' onClick={() => navigate('/')}>
                                Cancel
                            </Button>
                            <Button colorScheme="purple"
                                mr={3}
                                type="submit"
                                onClick={registerUsers}
                            >Submit</Button>
                        </ModalFooter>
                    </ModalContent>
                </Modal>
            </form>
        </>
    )
}