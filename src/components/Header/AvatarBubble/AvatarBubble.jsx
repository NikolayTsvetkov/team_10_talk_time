
import { Avatar, AvatarBadge, Badge } from "@chakra-ui/react";
import { useAuth } from "../../../contexts/AuthContext";

const AvatarBubble = () => {
  const {currentUserFullInfo} = useAuth();
//console.log(currentUserFullInfo?.status)
  return (
    <>
    {currentUserFullInfo?.status === 'busy' && <Badge ml='1' colorScheme='yellow'>Busy</Badge>}
    {currentUserFullInfo?.status === 'online' && <Badge ml='1' colorScheme='green'>Available</Badge>}
      <Avatar onClick={() => {console.log('Make me a profile look/card!')}} src={currentUserFullInfo?.photoURL}>
      {currentUserFullInfo?.status === 'busy' && <AvatarBadge boxSize='1.25em' bg='yellow'/>}
      {currentUserFullInfo?.status === 'offline' && <AvatarBadge borderColor='papayawhip' bg='tomato' boxSize='1.25em' />}
      
      </Avatar>
      {/* <p>{currentUserFullInfo.username}</p> */}
    </>
  );
};

export default AvatarBubble;




