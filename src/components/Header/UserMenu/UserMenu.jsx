import React from "react";
import {
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  Button,
  WrapItem,
  useDisclosure,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalBody,
  ModalFooter,
  Input,
  FormLabel,
  Textarea,
  Box,
  Select,
  Switch
} from "@chakra-ui/react";

import "./UserMenu.css";
import AvatarBubble from "../AvatarBubble/AvatarBubble";
import { profilePicturesRef } from "../../../config/firebase-config";
import { useAuth } from "../../../contexts/AuthContext";
import { useState } from "react";
import { uploadBytes, getDownloadURL, ref } from "firebase/storage";
import { updateUserUniversal } from "../../../services/users.service";
import { useNavigate } from "react-router-dom";

const UserMenu = () => {
  const context = useAuth();
  const navigate = useNavigate();

  const handleLogOut = () => {
    updateUserUniversal(context?.currentUser?.uid,
      "fields/status",
      'offline')
    context.logout();
    navigate("/");
  };

  function UploadPicture() {
    const { isOpen, onOpen, onClose } = useDisclosure();
    const [image, setImage] = useState(null);
    const [url, setUrl] = useState(null);

    const handleSelectPicture = (event) => {
      if (event.target.files[0]) {
        setImage(event.target.files[0]);
      }
    };

    const handleUpload = () => {
      const uniqueImageName = "photo of " + context.currentUser.uid;

      const imageRef = ref(profilePicturesRef, uniqueImageName);
      uploadBytes(imageRef, image)
        .then(() => {
          getDownloadURL(imageRef)
            .then((url) => {
              //console.log(url)
              setUrl(url);

              updateUserUniversal(
                context.currentUser.uid,
                "fields/photoURL",
                url
              );
            })
            .catch((e) => console.error(e.message));
        })
        .catch((e) => console.error(e.message));
    };

    return (
      <>
        <MenuItem onClick={onOpen} style={{ color: "#272849" }}>
          Upload/Change picture
        </MenuItem>
        <Modal
          isCentered
          onClose={onClose}
          isOpen={isOpen}
          motionPreset="slideInBottom"
        >
          <ModalOverlay />
          <ModalContent>
            <ModalHeader>Upload picture</ModalHeader>
            <ModalCloseButton />
            <ModalBody>
              <Input type="file" onChange={handleSelectPicture} />
            </ModalBody>
            <ModalFooter>
              <Button variant="ghost" onClick={onClose}>
                Cancel
              </Button>
              <Button colorScheme="purple" mr={3} onClick={handleUpload}>
                Upload
              </Button>
            </ModalFooter>
          </ModalContent>
        </Modal>
      </>
    );
  }

  function EditProfileModal() {
    const [updateObj, setUpdateObj] = useState({});
    const { isOpen, onOpen, onClose } = useDisclosure();

    const handleInput = (e) => {
      setUpdateObj({ ...updateObj, [e.target.name]: e.target.value });
      console.log(updateObj);
    };

    const handleUpdate = () => {
      if (updateObj.firstName) {
        //validation??
        updateUserUniversal(
          context.currentUser.uid,
          "fields/firstName",
          updateObj.firstName
        );
        console.log("firstname updated");
      }
      if (updateObj.lastName) {
        //validation??
        updateUserUniversal(
          context.currentUser.uid,
          "fields/lastName",
          updateObj.lastName
        );
        console.log("lastname updated");
      }
      if (updateObj.level) {
        updateUserUniversal(
          context.currentUser.uid,
          "fields/level",
          updateObj.level
        );
        console.log("Level updated");
      }
      if (updateObj.about) {
        updateUserUniversal(
          context.currentUser.uid,
          "fields/about",
          updateObj.about
        );
        console.log("About updated");
      }
    };

    return (
      <>
        <MenuItem style={{ color: "#272849" }} onClick={onOpen}>
          Edit Profile
        </MenuItem>
        <Modal
          isCentered
          onClose={onClose}
          isOpen={isOpen}
          motionPreset="slideInBottom"
        >
          <ModalOverlay />
          <ModalContent>
            <ModalHeader>Edit profile info</ModalHeader>
            <ModalCloseButton />
            <ModalBody>
              <FormLabel>First name</FormLabel>
              <Input
                onChange={handleInput}
                name="firstName"
                defaultValue={context.currentUserFullInfo.firstName}
              />

              <FormLabel>Last name</FormLabel>
              <Input
                onChange={handleInput}
                name="lastName"
                defaultValue={context.currentUserFullInfo.lastName}
              />

              <Box>
                <FormLabel>Change Level</FormLabel>
                <Select
                  defaultValue="beginner"
                  name="level"
                  onChange={handleInput}
                >
                  <option value="A1">Beginner(A1)</option>
                  <option value="A2">Elementary(A2)</option>
                  <option value="B1">Intermediate(B1)</option>
                  <option value="B2">Upper-intermediate(B2)</option>
                  <option value="C1">Advanced(C1)</option>
                  <option value="C2">Mastery(C2)</option>
                </Select>
              </Box>

              <FormLabel>About</FormLabel>
              <Textarea onChange={handleInput} name="about" />
            </ModalBody>
            <ModalFooter>
              <Button variant="ghost" onClick={onClose}>
                Cancel
              </Button>
              <Button colorScheme="purple" mr={3} onClick={handleUpdate}>
                Save
              </Button>
            </ModalFooter>
          </ModalContent>
        </Modal>
      </>
    );
  }

  const handleStatusChange = (e) => {
    if (context.currentUserFullInfo.status === 'online') {
      updateUserUniversal(context?.currentUser?.uid,
        "fields/status",
        'busy')
      navigate("/");
      return
    };
    updateUserUniversal(context?.currentUser?.uid,
      "fields/status",
      'online')
    navigate("/");
    return
  };

  return (
    <>
      <Menu>
        {/* <MenuButton as={Button} rightIcon={<ChevronDownIcon />}> */}
        <MenuButton className="avatar-btn" as={Button} colorScheme="white">
          ...
        </MenuButton>
        <MenuList>
<MenuItem
  color='black'
>
<FormLabel htmlFor='isInvalid'>{context.currentUserFullInfo.status === 'online' ? 'Quick talk enabled' : 'Quick talk disabled'}</FormLabel>
  <Switch defaultChecked colorScheme='purple' onChange={handleStatusChange}/>
</MenuItem>

        {/* <MenuItem
            onClick={handleStatusChange}
            color='black'
          >
            {context.currentUserFullInfo.status === 'available' ? 'Im not available for chat' : 'Im available for chat!'}
          </MenuItem> */}
          <EditProfileModal />

          <UploadPicture />

          <MenuItem
            style={{ color: "#272849", backgroundColor: "pink" }}
            onClick={handleLogOut}
          >
            Log out
          </MenuItem>
        </MenuList>
      </Menu>
      <WrapItem>
        <AvatarBubble />
        <p>{context.currentUser && context.currentUser.username}</p>
      </WrapItem>
    </>
  );
};

export default UserMenu;
