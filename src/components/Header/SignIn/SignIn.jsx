import {
    Button, useDisclosure, Modal, ModalOverlay, ModalContent, ModalHeader, ModalCloseButton, ModalBody, FormControl, FormLabel, Input, ModalFooter, GridItem
} from '@chakra-ui/react';
import { useState } from 'react';
import { useNavigate, NavLink } from 'react-router-dom';
import { useAuth } from '../../../contexts/AuthContext';
import { loginUser } from '../../../services/auth.service';
import { useValidation } from '../../../contexts/ValidationCOntext';
import { updateUserUniversal } from '../../../services/users.service';
//import { useEffect } from 'react';
import SignUpModal from '../SignUp/SignUp';


export default function SignInModal() {


    const { isOpen, onOpen, onClose } = useDisclosure()
    const [credentials, setCredentials] = useState({ email: '', password: '' });
    const { handleSetCurrentUser, currentUser } = useAuth();
    const navigate = useNavigate();
    const { errors, addError, clearErrors } = useValidation();
    
        
        
    


    const handleInputChange = e => {
        const { name, value } = e.target;
        setCredentials(prevCredentials => ({
            ...prevCredentials,
            [name]: value
        }));
    };

    const handleLogin = async () => {
        try {
            const user = await loginUser(credentials.email, credentials.password);
            handleSetCurrentUser(user?.userObj);
                if (user?.userObj?.uid) {
                    updateUserUniversal(user?.userObj?.uid,
                        "fields/status",
                        'online')
                }

        } catch (error) {
            console.log('Login error:', Object.keys(error), error.code, error.name, error.customData);

            addError(error.code)

        }
    };


    return (
        <>
            <GridItem color={'white'} ><NavLink onClick={onOpen} >LogIn</NavLink></GridItem>

            <Modal closeOnOverlayClick={false} isOpen={isOpen} onClose={onClose}>
                <ModalOverlay />
                <ModalContent>
                    <ModalHeader>Sign in</ModalHeader>
                    <ModalCloseButton />
                    <ModalBody pb={6}>
                        <FormControl>
                            <FormLabel>E-mail</FormLabel>
                            <Input
                                value={credentials.email}
                                onChange={handleInputChange}
                                name="email"
                                type="email"
                                placeholder='E-mail' />
                        </FormControl>

                        <FormControl mt={4}>
                            <FormLabel>Password</FormLabel>
                            <Input
                                value={credentials.password}
                                onChange={handleInputChange}
                                name="password"
                                type="password"
                                placeholder="Password"
                            />
                        </FormControl>
                        <br />
                        <p>You're not registered yet?</p>
                        <a onClick={() => navigate('/signup')}>Sign up now!</a>
                        
                    </ModalBody>

                    <ModalFooter>
                        <Button onClick={handleLogin} colorScheme='purple' mr={3}>
                            Sign in
                        </Button>
                        <Button onClick={onClose}>Cancel</Button>
                    </ModalFooter>
                </ModalContent>
            </Modal>
        </>
    )
}