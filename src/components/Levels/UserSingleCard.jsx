import { useState } from "react";
import {
  Box,
  Card,
  Image,
  Text,
  Button,
  HStack,
  VStack,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalBody,
  ModalFooter,
  FormControl,
  FormErrorMessage,
  Input,
  Textarea,
  useDisclosure,
} from "@chakra-ui/react";
import { createChat } from "../../services/chat.service";
import { useNavigate } from "react-router-dom";

function UserSingleCard({ user, currentUser }) {
  const navigate = useNavigate();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [titleError, setTitleError] = useState("");
  const [contentError, setContentError] = useState("");
  const [title, setTitle] = useState("");
  const [content, setContent] = useState("");

  const handleContentChange = (e) => {
    const newContent = e.target.value;
    setContent(newContent);

    if (newContent.length < 5 || newContent.length > 20) {
      setContentError("Content must be between 5 and 20 characters.");
    } else {
      setContentError("");
    }
  };

  const handleClose = () => {
    setTitle("");
    setContent("");
    onClose();
  };

  const handleClickMessage = () => {
    createChat(
      user.fields.username,
      content,
      currentUser.uid,
      currentUser.uid,
      user.uid
    );
    // navigate("/")
    onClose();
  };

  return (
    <Card
      bg="gray.800"
      color="white"
      boxShadow="md"
      borderRadius="md"
      p={4}
      transition="transform 0.2s"
      _hover={{ transform: "scale(1.05)" }}
    >
      <HStack spacing={4}>
        <Image
          src={user?.image || user?.fields.photoURL}
          alt={user.name}
          borderRadius="md"
          boxSize={24}
        />

        <VStack align="flex-start" spacing={2} flex={1}>
          <Text fontSize="md" fontWeight="bold" color="white" textAlign="right">
            {user?.name ||
              `${user?.fields?.firstName} ${user?.fields?.lastName}`}
          </Text>

          <Text fontSize="sm" fontWeight="bold" color="white" textAlign="right">
            Level: {user.level || user?.fields?.level}
          </Text>
        </VStack>
      </HStack>

      <Box my={4}>
        <Text>{user.about}</Text>
      </Box>

      <Button colorScheme="purple" size="sm" onClick={onOpen}>
        Send a Message
      </Button>

      <Modal
        isCentered
        onClose={onClose}
        isOpen={isOpen}
        motionPreset="slideInBottom"
      >
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>
            Message to:
            {user?.name ||
              ` ${user?.fields?.firstName} ${user?.fields?.lastName}`}
          </ModalHeader>
          <ModalCloseButton onClick={handleClose} />
          <ModalBody>
            <FormControl isInvalid={contentError}>
              <Textarea
                placeholder='Message will be add to "Personal Messages" as "Topic", content must be between 5 symbols and 20 symbols.'
                value={content}
                onChange={handleContentChange}
              />
              {contentError && (
                <FormErrorMessage>{contentError}</FormErrorMessage>
              )}
            </FormControl>
          </ModalBody>
          <ModalFooter>
            <Button colorScheme="blue" mr={3} onClick={handleClose}>
              Close
            </Button>
            <Button colorScheme="blue" mr={3} onClick={handleClickMessage}>
              Add
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </Card>
  );
}

export default UserSingleCard;
