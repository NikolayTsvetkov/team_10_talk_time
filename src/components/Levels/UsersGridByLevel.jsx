import { Grid, GridItem, Card, Image, Text, Button } from "@chakra-ui/react";
import UserSingleCard from "./UserSingleCard";
import { useAuth } from "../../contexts/AuthContext";
import { useState } from "react";
import CommonChat from "../Body/Chat/CommonChat";

function UsersGridByLevel() {
  const { currentUser, usersByLevel } = useAuth();
  const [showChat, setShowChat] = useState(false);
  const users = usersByLevel || [
    {
      id: 1,
      name: "John Doe",
      image: "https://randomuser.me/api/portraits/men/1.jpg",
      age: 25,
      country: "United States",
      level: "A1",
      about: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    },
    {
      id: 2,
      name: "Jane Smith",
      image: "https://randomuser.me/api/portraits/women/2.jpg",
      age: 30,
      country: "Canada",
      level: "A2",
      about: "Praesent sed nisi sed augue consectetur rutrum.",
    },

    {
      id: 3,
      name: "Patricia Bregovich",
      image: "https://randomuser.me/api/portraits/women/5.jpg",
      age: 33,
      country: "Canada",
      level: "A1",
      about: "Callistenic oprum de fruie mock hendu be",
    },
    {
      id: 4,
      name: "Emma Johnson",
      image: "https://randomuser.me/api/portraits/women/4.jpg",
      age: 58,
      country: "Australia",
      level: "B2",
      about: "Nulla ac diam ornare, condimentum elit id, lobortis elit.",
    },
    {
      id: 41,
      name: "Gerom Nurie",
      image: "https://randomuser.me/api/portraits/men/14.jpg",
      age: 24,
      country: "France",
      level: "B2",
      about: "Magallet yu minser wu cunnabile semi noop",
    },
    {
      id: 5,
      name: "Michael Brown",
      image: "https://randomuser.me/api/portraits/men/5.jpg",
      age: 35,
      country: "Germany",
      level: "C1",
      about: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem.",
    },
    {
      id: 6,
      name: "Sophia Wilson",
      image: "https://randomuser.me/api/portraits/women/6.jpg",
      age: 32,
      country: "France",
      level: "A1",
      about: "Fusce auctor lectus tellus, nec posuere mi lobortis in.",
    },
    {
      id: 7,
      name: "David Thompson",
      image: "https://randomuser.me/api/portraits/men/7.jpg",
      age: 41,
      country: "United Kingdom",
      level: "C2",
      about: "Vivamus vel lacus vel leo porttitor hendrerit.",
    },
  ];
  const handleToggleChat = () => {
    setShowChat(!showChat);
    if (usersByLevel?.length < 1) {
      return (
        <Text
          fontSize="xl"
          fontWeight="bold"
          color="purple.500"
          textAlign="center"
          mt={8}
        >
          No users in this category
        </Text>
      );
    }
  };

  return (
    <>
      <Button
        onClick={handleToggleChat}
        colorScheme="purple"
        mt={1}
        alignSelf="top"
        px={8}
        mx={2}
      >
        {!showChat ? "Show Chat" : "Back to Users"}
      </Button>
      {!showChat ? (
        <Grid templateColumns="repeat(4, 1fr)" gap={6} p={6}>
          {users.map((user) => (
            <GridItem key={user.uid}>
              <UserSingleCard user={user} currentUser={currentUser} />
            </GridItem>
          ))}
        </Grid>
      ) : (
        <CommonChat
          width="100%"
          users={usersByLevel}
          currentUser={currentUser}
        />
      )}
    </>
  );
}

export default UsersGridByLevel;
